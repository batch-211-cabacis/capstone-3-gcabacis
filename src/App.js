// import React from 'react';
import './App.css';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import {useState, useEffect} from 'react';
import {UserProvider} from './UserContext';
import AppNavbar from './components/AppNavbar';
import ProductView from './components/ProductView';
import AddProduct from './components/AddProduct';
import AdminDash from './components/AdminDash';
import EditProduct from './components/EditProduct';
import Products from './pages/Products';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login'
import Logout from './pages/Logout';
import PageNotFound from './pages/PageNotFound';
// import UserOrder from './components/UserOrder';

// import Footer from './components/Footer';


function App() {


  const [ user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () =>{
    localStorage.clear()
  }

  useEffect(()=>{
    fetch('https://galleriabyangelique-app.onrender.com/users/details',{
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res=>res.json())
    .then(data=>{
    console.log(data)
    if(typeof data._id !== "undefined"){
      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      })
    }else{
      setUser({
        id: null,
        isAdmin: null
      })
    }
  })
  },[])

  return (


    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar/>
          <Container>
            <Routes>
              <Route path="/" element={<Home/>}/>
              <Route path="/admin" element={<AdminDash/>}/>
              {/*<Route path="/userOrder" element={<UserOrder/>}/>*/}
              <Route path="/products" element={<Products/>}/>
              <Route path="/products/:productId" element={<ProductView/>}/>
              <Route path="/editProduct/:productId" element={<EditProduct/>} />
              <Route path="/addProduct" element={<AddProduct/>} />
              <Route path="/login" element={<Login/>}/>
              <Route path="/register" element={<Register/>}/>
              <Route path="/logout" element={<Logout/>}/>
              <Route path="*" element={<PageNotFound/>}/>
            </Routes>
          </Container>
            {/*// {<Footer/>}*/}
      </Router>
    </UserProvider>

  );
}

export default App;
