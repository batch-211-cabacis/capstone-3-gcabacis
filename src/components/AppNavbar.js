import {Navbar, Nav, Container} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import {useContext} from 'react';
import UserContext from '../UserContext';

export default function AppNavbar(){

	const {user} = useContext(UserContext);

	return(

	<Navbar>
      	<Container>
   <Navbar.Brand href="/">
            <img 
              src='https://i.imgur.com/XU8RM1F.png'
              width="200"
              alt="logo"
            />
          </Navbar.Brand >
        	{/*<Navbar.Brand style={{color: 'rgba(240, 96, 96, 0.5)'}} as={Link} to="/">Galleria by Angelique</Navbar.Brand>*/}
        		<Navbar.Toggle aria-controls="basic-navbar-nav" />
        			<Navbar.Collapse id="basic-navbar-nav">
          			<Nav className="me-auto">
            		<Nav.Link className="fs-5 fw-bold" style={{color: 'rgba(240, 96, 96, 0.5)'}} as={Link} to="/" >Home</Nav.Link>
            		<Nav.Link className="fs-5 fw-bold" style={{color: 'rgba(240, 96, 96, 0.5)'}} as={Link} to="/products">Menu</Nav.Link>
            		{(user.id !== null) ?
            		<Nav.Link className="fs-5 fw-bold" style={{color: 'rgba(240, 96, 96, 0.5)'}} as={Link} to="/logout">Logout</Nav.Link>
            		:
            		<>
            		<Nav.Link className="fs-5 fw-bold" style={{color: 'rgba(140, 190, 178, 0.5)'}} as={Link} to="/login">Login</Nav.Link>
            		<Nav.Link className="fs-5 fw-bold" style={{color: 'rgba(140, 190, 178, 0.5)'}} as={Link} to="/register">Register</Nav.Link>
            		</>
            		}
          		</Nav>
        	</Navbar.Collapse>
      	</Container>
    </Navbar>
	)
}



