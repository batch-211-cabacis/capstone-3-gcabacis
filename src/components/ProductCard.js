import {Card, Button, Container, Row, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function ProductCard({productProp}){
	
	let { name, description, image, price, _id} = productProp;

	return(
		<Col xs={6} md={4}>
		<Card>
		<Card.Img  src={image} width={200} height={300}/>
			<Card.Body className="box-height" style={{ background: 'linear-gradient(to right, rgba(140, 190, 178, 0.5), rgba(240, 96, 96, 0.5))'}}>
				<Card.Title className="mt-2" class="fs-3" style={{color: 'white'}}>{name}</Card.Title>
				<Button className='btn btn-light float-right mt-3' style={{ background: 'linear-gradient(to left, rgba(140, 190, 178, 0.5), rgba(240, 96, 96, 0.5))', color: 'white' }} as ={Link} to={`/products/${_id}`}>Click here to order</Button>
			</Card.Body>

		</Card>
		</Col>
	)
}







			