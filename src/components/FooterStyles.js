import styled from 'styled-components';
   
export const Box = styled.div`
  padding: 50px 5px 0 5px;
  background: linear-gradient(to right, rgba(140, 190, 178, 0.5), rgba(240, 96, 96, 0.5));
  width: 100%;
  position: relative;
  display: block;
  clear: both;
  margin: 30px 0 0 0;
  
   
//   @media (max-width: 1000px) {
//     padding: 50px 30px;
//   }
// `;
   
export const Container = styled.div`
    display:;
    flex-direction: column;
    justify-content: center;
    max-width: 1000px;
    margin: auto;
    
`
   
export const Column = styled.div`
  // display: flex;
  // flex-direction: column;
  // text-align: left;
  // margin-left: 60px;
`;
   
export const Row = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, 
                         minmax(185px, 1fr));
  grid-gap: 100px;
   
  @media (max-width: 1000px) {
    grid-template-columns: repeat(auto-fill, 
                           minmax(200px, 1fr));
  }
`;
   
export const FooterLink = styled.a`
  color: #fff;
  margin-bottom: 10px;
  font-size: 18px;
  text-decoration: none;
   
  &:hover {
      color: #8CBEB2;
      transition: 200ms ease-in;
  }
`;
   
export const Heading = styled.p`
  font-size: 24px;
  color: #fff;
  margin-bottom: 40px;
  font-weight: bold;
`;