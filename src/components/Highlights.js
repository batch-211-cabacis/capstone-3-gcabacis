import {Row, Col, Card, Button} from 'react-bootstrap';
import {useParams, Link, useNavigate} from 'react-router-dom';



export default function Highlights(){


  return (
      <Row className="mt-1 mb-1" >


        <Col xs={12} md={4} >
        
          <Card style={{ margin: '50px 0', background: 'linear-gradient(to right, rgba(140, 190, 178, 0.5), rgba(240, 96, 96, 0.5))' }}>
          <Card.Img  src="https://images.pexels.com/photos/6798402/pexels-photo-6798402.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" width={200} height={300}/>
              <Card.Body className="box-height">
                <Card.Title style={{color: 'white'}}>Cakes and Pastries</Card.Title>
                <Card.Text style={{color: 'white'}}>
                  In general, for future efficacy, if someone asks you the difference between cake and pastry, you could say this: A cake is a baked good made with leaveners, flour, fat, and sugar, and a pastry is a dough paste made primarily with flour and fat. And is decorated with icing and forsting and sugar candies. 
                </Card.Text>
                <Button className="btn btn-light" style={{ background: 'linear-gradient(to left, rgba(140, 190, 178, 0.5), rgba(240, 96, 96, 0.5))', color: 'white' }} as={Link} to={`/products`}>Browse Menu</Button>
              </Card.Body>
            </Card>
        </Col>


        <Col xs={12} md={4}>
          <Card style={{ margin: '50px 0', background: 'linear-gradient(to right, rgba(140, 190, 178, 0.5), rgba(240, 96, 96, 0.5))' }}>
          <Card.Img  src="https://images.pexels.com/photos/769969/pexels-photo-769969.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" width={200} height={300}/>
              <Card.Body className="box-height">
                <Card.Title style={{color: 'white'}}>Pasta</Card.Title>
                <Card.Text style={{color: 'white'}}>
                  The word “pasta” itself derives from the Italian word for the dough from which pasta shapes are made. The names themselves usually trace back to either their creation process or objects they resemble. “Spaghetti”, for example, derives from the Italian word spago, meaning string, resembling its long, straggly shape.
                </Card.Text>
                <Button className="btn btn-light" style={{ background: 'linear-gradient(to left, rgba(140, 190, 178, 0.5), rgba(240, 96, 96, 0.5))', color: 'white' }} as={Link} to={`/products`}>Browse Menu</Button>
              </Card.Body>
            </Card>
        </Col>

        <Col xs={12} md={4}>
          <Card style={{ margin: '50px 0', background: 'linear-gradient(to right, rgba(140, 190, 178, 0.5), rgba(240, 96, 96, 0.5))' }}>
          <Card.Img  src="https://images.pexels.com/photos/1833333/pexels-photo-1833333.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" width={200} height={300}/>
              <Card.Body className="box-height">
                <Card.Title style={{color: 'white'}}>Salad</Card.Title>
                <Card.Text style={{color: 'white'}}>
                  sal·​ad ˈsa-ləd. : any of various usually cold dishes: such as. : raw greens (such as lettuce) often combined with other vegetables and toppings and served especially with dressing. : small pieces of food (such as pasta, meat, fruit, or vegetables) usually mixed with a dressing (such as mayonnaise) or set in gelatin.
                </Card.Text>
                <Button className="btn btn-light" style={{ background: 'linear-gradient(to left, rgba(140, 190, 178, 0.5), rgba(240, 96, 96, 0.5))', color: 'white' }} as={Link} to={`/products`}>Browse Menu</Button>
              </Card.Body>
            </Card>
        </Col>


        

      </Row>      
    )
}



    
