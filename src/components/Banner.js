import {Row, Col, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Banner({bannerProp}){

	const {title, content, destination, label} = bannerProp;

	return(
		<div className= "text-center mt-5" >
		<Row>
			<Col>
				<h1 className="mt-5" style={{color: 'white'}} >{title}</h1>
				<h4 className="mt-5 mb-5" style={{color: 'white'}}>{content}</h4>
				<Button as={Link} to={destination} className="btn btn-light mt-5 mb-5" style={{ background: 'linear-gradient(to left, rgba(140, 190, 178, 0.5), rgba(240, 96, 96, 0.5))', color: 'white' }}>{label}</Button>
			</Col>	
		</Row>
		</div>
	)
}