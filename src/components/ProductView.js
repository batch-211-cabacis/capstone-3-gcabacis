import {useState, useEffect, useContext} from 'react';
import {Container, Card, Button, Row, Col} from 'react-bootstrap';
import {useParams, Link, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function ProductView(){

	const {user} =useContext(UserContext);

	const navigate = useNavigate();

	const {productId} = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [image, setImage] = useState("");
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(0)

	const order = (productProp)=>{

		let { name, description, price, _id } = productProp;

		fetch('https://galleriabyangelique-app.onrender.com/users/order',{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res=>res.json())
		.then(data=>{
			if(data===true){
			Swal.fire({
            title: "Order successful!",
            icon: "succcess",
            text: "Thank you for your order"
          })

			navigate("/products")

			}else{
			Swal.fire({
            title: "Ooops something went wrong",
            icon: "error",
            text: "Please check your credentials"
          })
		}
		})
	}



	useEffect(()=>{
		console.log(productId)
		fetch(`https://galleriabyangelique-app.onrender.com/products/${productId}`)
		.then(res=>res.json())
		.then(data=>{
			setName(data.name)
			setImage(data.image)
			setDescription(data.description)
			setPrice(data.price)
			setQuantity(data.quantity)
		})
	},[productId, quantity])

	return(
		<Container className="mt-5">
			<Row>
			
				<Col md={{ span: 6, offset: 3 }}>
					<Card style={{ background: 'linear-gradient(to right, rgba(140, 190, 178, 0.5), rgba(240, 96, 96, 0.5))' }}>

						<Card.Body>
						<Card.Img  src={image} width={400} height={400}/>
							<Card.Title className="mt-5" class="fs-3" style={{color: 'white'}}>{name}</Card.Title>
							<Card.Subtitle className="mt-4" style={{color: 'white'}}>Description:</Card.Subtitle>
							<Card.Text className="mt-2" style={{color: 'white'}}>{description}</Card.Text>
							<Card.Subtitle className="mt-4" class="fs-5" style={{color: 'white'}}>Price:</Card.Subtitle>
							<Card.Text style={{color: 'white'}} class="fs-5">PHP {price}</Card.Text>
							{
								(user.id !== null)?
								<Button variant="primary" onClick={()=>order(productId)}>Order</Button>
								:
								<Link className="btn btn-light" style={{ background: 'linear-gradient(to left, rgba(140, 190, 178, 0.5), rgba(240, 96, 96, 0.5))', color: 'white' }} to="/login">Login to order</Link>
							}	
						</Card.Body>

					</Card>
				</Col>
			</Row>
		</Container>
	)
}

