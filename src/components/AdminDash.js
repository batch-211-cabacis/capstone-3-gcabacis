import { useContext, useState, useEffect } from "react";
import {Table, Button} from "react-bootstrap";
import {Navigate, Link} from "react-router-dom";
import UserContext from "../UserContext";

import Swal from "sweetalert2";

export default function AdminDash(){

	
	const {user} = useContext(UserContext);

	
	const [allProducts, setAllProducts] = useState([]);

	
	const fetchData = () =>{
		
		fetch(`https://galleriabyangelique-app.onrender.com/products/all`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllProducts(data.map(product => {
				return(
					<tr key={product._id}>
						<td>{product._id}</td>
						<td>{product.name}</td>
						<td><img src={product.image} width={200} height={200} /></td>
						<td>{product.description}</td>
						<td>{product.price}</td>
						<td>{product.isActive ? "Active" : "Inactive"}</td>
						<td>
							{
								
								(product.isActive)
								?	
								 	
									<Button style={{ background: 'linear-gradient(to right, rgba(140, 190, 178, 0.5), rgba(240, 96, 96, 0.5))' }} size="sm" onClick ={() => archive(product._id, product.name)}>Deactivate</Button>
								:
									<>
										
										<Button style={{ background: 'linear-gradient(to right, rgba(140, 190, 178, 0.5), rgba(240, 96, 96, 0.5))' }} size="sm" onClick ={() => unarchive(product._id, product.name)}>Activate</Button>
										
										<Button as={ Link } to={`/editProduct/${product._id}`} variant="primary" size="sm" className="m-2" >Update</Button>
									</>
							}
						</td>
					</tr>
				)
			}))

		})
	}


		

	
	const archive = (productId, productName) =>{
		console.log(productId);
		console.log(productName);

		fetch(`https://galleriabyangelique-app.onrender.com/products/${productId}/archive`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: "Archive Succesful!",
					icon: "success",
					text: `${productName} is now inactive.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Archive Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	
	const unarchive = (productId, productName) =>{
		console.log(productId);
		console.log(productName);

		fetch(`https://galleriabyangelique-app.onrender.com/products/${productId}/archive`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: "Unarchive Succesful!",
					icon: "success",
					text: `${productName} is now active.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Unarchive Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}
	
	useEffect(()=>{
		
		fetchData();
	})
	
	return(
		(user.isAdmin)
		?
		<>
			<div className="mt-1 mb-3 text-center">
				<h1 className="mt-2 mb-2">Admin Dashboard</h1>
		
				<Button  as={Link} to="/addProduct" style={{ background: 'linear-gradient(to right, rgba(140, 190, 178, 0.5), rgba(240, 96, 96, 0.5))' }} size="lg" className="mx-2 mb-5">Add Product</Button>
				
			</div>
			<Table striped bordered hover>
		     <thead>
		       <tr>
		         <th>Product ID</th>
		         <th>Product Name</th>
		         <th>Product Image</th>
		         <th>Description</th>
		         <th>Price</th>
		         <th>Status</th>
		         <th>Action</th>
		       </tr>
		     </thead>
		     <tbody>
		       { allProducts }
		     </tbody>
		   </Table>
		</>
		:
		<Navigate to="/products" />
	)
}





// // END...........


