const productsData = [
		{
			id: "001" ,
			name: "Chocolate Cake",
			description: "Sample Text",
			price: "PHP 400",
			onOffer: true
		},

		{
			id: "002" ,
			name: "Strawberry Short Cake",
			description: "Sample Text",
			price: "PHP 400",
			onOffer: true
		},

		{
			id: "003" ,
			name: "Carrot Cake",
			description: "Sample Text",
			price: "PHP 400",
			onOffer: true
		}

		
	]

export default productsData;
