import Banner from '../components/Banner';


export default function PageNotFound(){

	const data = {
		title: "404 - Page Not Found",
		content: "Oooops! This page is not available",
		destination: "/",
		label: "Back home"
	}

	return(
		<Banner bannerProp={data}/>
	)
}