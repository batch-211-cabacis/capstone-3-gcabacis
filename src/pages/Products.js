 // import  productsData from '../data/products'
 import ProductCard from '../components/ProductCard'
 import {useEffect, useState, useContext} from 'react';
 import { Navigate } from 'react-router-dom';
 import UserContext from '../UserContext';
 import {Row} from 'react-bootstrap'; 


 export default function Products(){

    const [products, setProducts] = useState([]);

    const {user} = useContext(UserContext);
    console.log(user);

    useEffect(()=>{
        fetch('https://galleriabyangelique-app.onrender.com/products')
        .then(res=>res.json())
        .then(data=>{

        const productArr = (data.map((product)=>{
        return (
            <ProductCard productProp={product} key={product._id}/>
            )
        }))
        setProducts(productArr)
    })
    },[products])

 	return(
        (user.isAdmin)?
        <Navigate to="/admin"/>
        :
 		<>
 		<h1 style={{color: 'white' , background: 'linear-gradient(to right, rgba(140, 190, 178, 0.5), rgba(240, 96, 96, 0.5))'}} className="mt-5 mb-5 text-center">Enjoy our Menu!</h1>
 		<Row>
        {products}
        </Row>
 		</>
 	)
}