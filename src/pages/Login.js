import {Form, Button, Container, Row, Col, Card} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import {Navigate} from 'react-router-dom';
import Swal from 'sweetalert2';
// import Image from "react-bootstrap";


export default function Login(){

	
  	const [ email, setEmail ] = useState('');
  	const [ password, setPassword] = useState('');
  	const [ isActive, setIsActive ] = useState('');


  	console.log(email);
  	console.log(password);


  	const {user, setUser} = useContext(UserContext)

  	
  	function authenticate(e){
  		e.preventDefault()

      fetch('https://galleriabyangelique-app.onrender.com/users/login',{
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            email: email,
            password: password
          })
      })
      .then(res=>res.json())
      .then(data=>{
        console.log(data)
        if(typeof data.access !== "undefined"){
          localStorage.setItem('token', data.access)
          retrieveUserDetails(data.access)

          Swal.fire({
            title: "Login successful!",
            icon: "succcess",
            text: "Welcome to Galleria by Angelique!"
          })
        }else{
          Swal.fire({
            title: "Athentication failed",
            icon: "error",
            text: "Please check your credentials."
          })
        }
      })


      const retrieveUserDetails = (token)=>{
          fetch('https://galleriabyangelique-app.onrender.com/users/details',{
            headers: {
              Authorization: `Bearer ${token}`
            }
          })
          .then(res=>res.json())
          .then(data=>{
            console.log(data)

            setUser({
              id: data._id,
              isAdmin: data.isAdmin
            })
          })
        }

  		setEmail("");
  		setPassword("");

  		// alert(`${email} has been verified! Welcome back muncher!`)
  	}

  	useEffect(()=>{
  		if(email !== "" && password !== ""){
  			setIsActive(true)
  		}else{
  			setIsActive(false)
  		}
  	},[email, password])

	return(

	(user.id !== null)
	?
	<Navigate to="/"/>
	:
    <Container>
    <Row>
    <Col md={{ span: 6, offset: 3 }}>
    <Col>
    <Card className="my-5" style={{ background: 'linear-gradient(to right, rgba(140, 190, 178, 0.5), rgba(240, 96, 96, 0.5))' }}>
    <Row className="g-0">
    <Col md="11" className="d-none d-md-block m-4">
    <Form className="mt-1" onSubmit={(e)=>authenticate(e)}>
    <h2 className="mb-4" style={{color: 'rgba(240, 96, 96, 0.5)'}}>Log in to your account</h2>

      <Form.Group className="mb-3" controlId="userEmail">
        <Form.Label style={{color: 'rgba(240, 96, 96, 0.5)'}}>Email address</Form.Label>
        <Form.Control 
        type="email" 
        placeholder="Enter email"
        value={email} 
        onChange={e=>setEmail(e.target.value)}
        required/>
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="password1">
        <Form.Label style={{color: 'rgba(240, 96, 96, 0.5)'}}>Password</Form.Label>
        <Form.Control 
        type="password" 
        placeholder="Enter Password" 
        value={password} 
        onChange={e=>setPassword(e.target.value)}
        required/>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicCheckbox">
        <Form.Check type="checkbox" label="Remember me" />
      </Form.Group>
      
      { isActive ?
      <Button style={{ background: 'linear-gradient(to left, rgba(140, 190, 178, 0.5), rgba(240, 96, 96, 0.5))' }} type="submit" id="submitBtn">
        Login
      </Button>
      :
      <Button variant="danger" type="submit" id="submitBtn" disabled>
        Login
      </Button>
      }

    </Form>
    </Col>
    </Row>
    </Card>
    </Col>
    </Col>
    </Row>
    </Container>
	)
}