import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import {Row} from 'react-bootstrap';

export default function Home(){

	const data = {
		
		title: "Hungry or Hangry?",
		content: "I feel you! Worry no more! We've got you covered.",
		destination: "/products",
		label: "Click me to satisfy"
		
	}

	return(
		<Row>
		<div  style={{ margin: '50px 0', background: 'linear-gradient(to right, rgba(140, 190, 178, 0.5), rgba(240, 96, 96, 0.5))' }} >
		<Banner bannerProp={data} />

		</div>
		<Highlights/>
		</Row>


	)
}







