import { Form, Button, Container, Row, Col, Card } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import {Link} from 'react-router-dom';

export default function Register() {

 
  const { user } = useContext(UserContext);
  const navigate = useNavigate();

    
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email,setEmail] = useState('');
  const [mobileNo, setMobileNo] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');

  const [isActive, setIsActive] = useState(false);

  console.log(firstName);
  console.log(lastName);
  console.log(email);
  console.log(mobileNo);
  console.log(password1);
  console.log(password2);

    
    function registerUser(e){
      e.preventDefault();

      fetch(`https://galleriabyangelique-app.onrender.com/users/checkEmail`,{
        method: "POST",
        headers:{
          'Content-Type':'application/json'
        },
        body: JSON.stringify({
          email: email
        })
      })
      .then(res=>res.json())
      .then(data=>{
        console.log(data)

        if(data === true){

            Swal.fire({
            title: "Duplicate Email Found!",
            icon: "error",
            text: "Kindly provide another email to complete the registration!"
          })

        }else{

          fetch(`https://galleriabyangelique-app.onrender.com/users/register`,{
            method:"POST",
            headers:{
              'Content-Type':'application/json'
            },
            body: JSON.stringify({
              firstName: firstName,
              lastName: lastName,
              email: email,
              mobileNo: mobileNo,
              password: password1
            })
          })
          .then(res=>res.json())
          .then(data=>{


            if(data===true){

              setFirstName("");
              setLastName("");
              setEmail("");
              setMobileNo("");
              setPassword1("");
              setPassword2("");

              Swal.fire({
                title: "Registration successful!",
                icon: "success",
                text: "Welcome to Galleria by Angelique"
              })

              
              navigate("/login")

            }else{

              Swal.fire({
                title: "Something went wrong!",
                icon: "error",
                text: "Please try again!"

            })
          }
        })
      }
    })
  }


  useEffect(()=>{

    if((firstName !=="" && lastName !== "" && mobileNo.length === 11 && email !=="" && password1 !=="" && password2 !=="")&&(password1===password2)){
      setIsActive(true)
    }else{
      setIsActive(false)
    }

  },[firstName,lastName,email,mobileNo,password1,password2])


  return (
    (user.id !== null)
    ?
    <Navigate to="/login"/>
    :
    <Container>
   
    <Col md={{ span: 6, offset: 3 }}>

    
    <Card className="my-5" style={{ background: 'linear-gradient(to right, rgba(140, 190, 178, 0.5), rgba(240, 96, 96, 0.5))' }}>
    <Row className="g-0">
    <Col md="11" className="d-none d-md-block m-4">




    <Form className="mt-1" onSubmit={(e)=>registerUser(e)}>
    <h2 className="mb-4" style={{color: 'rgba(240, 96, 96, 0.5)'}}>Create an Account</h2>
      <Form.Group className="mb-3" controlId="firstName">
        <Form.Label style={{color: 'rgba(240, 96, 96, 0.5)'}}>First Name</Form.Label>
        <Form.Control
        type="text"
        placeholder="Enter First Name"
        value={firstName}
        onChange={e=>setFirstName(e.target.value)}
        required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="lastName">
        <Form.Label style={{color: 'rgba(240, 96, 96, 0.5)'}}>Last Name</Form.Label>
        <Form.Control
        type="text"
        placeholder="Enter Last Name"
        value={lastName}
        onChange={e=>setLastName(e.target.value)}
        required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="userEmail">
        <Form.Label style={{color: 'rgba(240, 96, 96, 0.5)'}}>Email address</Form.Label>
        <Form.Control
        type="email"
        placeholder="Enter email"
        value={email}
        onChange={e=>setEmail(e.target.value)}
        required
        />
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="mobileNo">
        <Form.Label style={{color: 'rgba(240, 96, 96, 0.5)'}}>Mobile Number</Form.Label>
        <Form.Control
        type="text"
        placeholder="Enter Mobile Number"
        value={mobileNo}
        onChange={e=>setMobileNo(e.target.value)}
        required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password1">
        <Form.Label style={{color: 'rgba(240, 96, 96, 0.5)'}}>Password</Form.Label>
        <Form.Control
        type="password"
        placeholder="Password"
        value={password1}
        onChange={e=>setPassword1(e.target.value)}
        required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password2">
        <Form.Label style={{color: 'rgba(240, 96, 96, 0.5)'}}>Password</Form.Label>
        <Form.Control
        type="password"
        placeholder="Verify Password"
        value={password2}
        onChange={e=>setPassword2(e.target.value)}
        required/>
      </Form.Group>
     

        { isActive ?      
            <Button className="mt-3" style={{ background: 'linear-gradient(to left, rgba(140, 190, 178, 0.5), rgba(240, 96, 96, 0.5))' }} type="submit" id="submitBtn">
                Register
            </Button>
          :
            <Button className="mt-3" variant="danger" type="submit" id="submitBtn" disabled>
                Register
            </Button>
        }

        <p class="fs-6 mt-3" style={{color: 'rgba(240, 96, 96, 0.5)'}}>Already have an account? <a href="/login" >  Click here</a> to login</p>
    </Form>
    </Col>
    </Row>
    
    </Card>
    </Col>
  
   
    </Container>
  );
}



